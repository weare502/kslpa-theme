#footer {
	color: $white;
	background: $blue url('static/images/footer-mask.png');
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	overflow: hidden;

	a {
		color: $white;
	}
}

.footer-quicklinks {
	background: $dark-blue;
	padding-top: .5rem;
	padding-bottom: .5rem;
	margin-bottom: 2rem;

	.u-wrapper {
		display: flex;
		justify-content: space-between;
		align-items: center;

		img {
			height: 1.5rem;

			@include mq($tablet) {
				height: auto;
			}
		}
	}

	&__item {
		display: inline-flex;
		text-decoration: none;
		flex-direction: column;
		align-content: center;

		@include mq($from: 448px) {
			flex-direction: row;
			align-items: center;
		}

		svg {
			display: none;

			@include mq(448px) {
				display: inline-block;
				margin-right: .5rem;
				height: 1.5rem;
			}

			@include mq($tablet) { height: auto; }
		}
		

		.nmls-num {
			display: inline-block;
			//padding-left: 4.8rem;
			@include mq($from: 448px) { padding-left: 3.2rem; }
		}
	}

	a {
		text-decoration: none;
		&:hover {
			text-decoration: underline;
		}
	}
}

.footer-inner-wrapper {
	@include mq($tablet){
		display: flex;
		flex-wrap: wrap;
	}
}

.footer-location-info {
	max-width: 48.5rem;
	width: 100%;
	display: flex;
	flex-wrap: wrap;

	&__item {
		margin-right: 3rem;
		margin-bottom: 2rem;

		.app-store-icons {
			width: 6.125rem;
		}
	}

	a {
		text-decoration: none;
		&:hover {
			text-decoration: underline;
		}
	}
}

.footer-menu-wrapper {

	@include mq($tablet){
		margin-left: 1.2rem;
	}

	ul {
		list-style: none;
		margin: 0;
		padding: 0;

		a {
			text-decoration: none;
			&:hover {
				text-decoration: underline;
			}
		}
	}

}

.footer-header {
	font-family: $font-heading;
	letter-spacing: .25rem;
	font-weight: 700;
	margin-bottom: 1rem;
	text-transform: uppercase;
}

.footer-meta {
	border-top: 2px solid rgba($grey-border, 0.4);
	margin-top: 1rem;
	
	@include mq($tablet){
		display: flex;
		align-items: center;
	}

	> * {
		@include mq($tablet){
			max-width: 25rem;
			width: 100%;
		}
	}

	.copyright {
		margin-top: 1rem;
		text-align: left;
		@include mq($tablet){
			text-align: center;
			margin-top: 0;
		}
	}

	.agency {
		margin-top: 1rem;
		text-align: left;
		@include mq($tablet){
			text-align: right;
			margin-top: 0;
		}
		img {
			vertical-align: middle;
		}
	}
}