module.exports = {
  theme: {
    extend: {
      screens: {
        'xl': '1200px'
      },
      inset: {
        'full': '100%'
      },
      minWidth: {
        64: '16rem'
      },
      fontSize: {
        '6xl': '60px'
      },
    },
    fontFamily: {
      body: 'Poppins, sans-serif',
      display: 'Playfair Display, serif',
      mont: 'Montserrat, Arial, sans-serif',
    },
    colors: {
      blue: {
        darkest: '#002250',
        dark: '#1B3665',
        default: '#094E8B',
        light: '#0465A8',
        lightest: '#097ABC'
      },
      lightblue: '#E9F7FF',
      gray: {
        light: '#ECECEC',
        default: '#595959',
        dark: '#474747'
      },
      grey: {
        light: '#ECECEC',
        default: '#595959',
        dark: '#474747'
      },
      black: '#000',
      white: '#FFF',
      transparent: 'transparent'
    }
  },
  variants: {
    display: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    fontWeight: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    boxShadow: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    backgroundColor: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    background: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
    cursor: ['responsive', 'hover', 'focus', 'group-hover', 'focus-within'],
  },
  plugins: []
}
