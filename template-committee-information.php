<?php

/**
 * Template Name: Committee Information Template
 */

$context = Timber::get_context();

$context['post'] = Timber::get_post();

Timber::render( 'committee.twig', $context );
