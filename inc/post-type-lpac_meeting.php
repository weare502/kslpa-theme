<?php

$labels = array(
	'name'               => __( 'LPAC Meetings', 'spha' ),
	'singular_name'      => __( 'LPAC Meeting', 'spha' ),
	'add_new'            => _x( 'Add New LPAC Meeting', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New LPAC Meeting', 'spha' ),
	'edit_item'          => __( 'Edit LPAC Meeting', 'spha' ),
	'new_item'           => __( 'New LPAC Meeting', 'spha' ),
	'view_item'          => __( 'View LPAC Meeting', 'spha' ),
	'search_items'       => __( 'Search LPAC Meetings', 'spha' ),
	'not_found'          => __( 'No LPAC Meetings found', 'spha' ),
	'not_found_in_trash' => __( 'No LPAC Meetings found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent LPAC Meeting:', 'spha' ),
	'menu_name'          => __( 'LPAC Meetings', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-calendar',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => true,
	'query_var'           => true,
    'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title',
	),
);

register_post_type( 'lpac_meeting', $args );