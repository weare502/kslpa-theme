<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['featured'] = Timber::get_posts([
    'posts_per_page' => 3,
    'post_type' => 'audit',
    'meta_key' => 'featured',
    'meta_value' => true
]);

$context['next_meeting'] = false;

$next = Timber::get_posts([
    'post_type' => 'lpac_meeting',
    'post_status' => ['future'],
    'order' => 'ASC',
    'posts_per_page' => 1
]);

if ($next){
    $context['next_meeting'] = $next[0];
}


$old = Timber::get_posts([
    'post_type' => 'lpac_meeting',
    'posts_per_page' => 1
]);

$context['past_meeting'] = $old[0];

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );