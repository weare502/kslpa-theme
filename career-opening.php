<?php
/**
 * Template Name: Career Opening Template
 */

$context = Timber::get_context();

$context['post'] = Timber::get_post();

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( 'opening.twig', $context );
}