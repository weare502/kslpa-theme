<?php
/**
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();

$context['post'] = $post;

// $related = Timber::get_posts([
//     'post_type' => 'audit',
//     'post__not_in' => [$post->ID],
//     'posts_per_page' => 3,
//     'tax_query' => [
//         [
//             'taxonomy' => 'audit_subject',
//             'terms' => array_map(function($o){
//                 return $o->ID;
//             }, $post->terms('audit_subject')),
//         ]
//     ],
// ]);

// $relatedCount = count($related);

// if ( $relatedCount < 3 ){
//     $related[] = Timber::get_posts([
//         'post_type' => 'audit',
//         'posts_per_page' => 3 - $relatedCount,
//         'post__not_in' => [$post->ID],
//     ]);
// }

// $context['related'] = $related;

if ( post_password_required( $post->ID ) ) {
    Timber::render( 'single-audit-password.twig', $context );
} else {
    Timber::render( 'single-audit.twig', $context );
}